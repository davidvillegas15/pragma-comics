(function(){
	app.controller('AdministrationUsersController', controller); 
	function controller($scope, SessionService, SecurityService, UserFactory, ToastService){
		if(SecurityService.isAdministrator()){
			$scope.users = UserFactory.find();
			$scope.cleanForm = function(){
				$scope.user = {};
			}; 
			$scope.edit = function(id){
				$scope.user = UserFactory.find(function(user){
					return user.id==id;
				});
			}
			$scope.save = function(id){
				var user = $scope.user;
				if(user.id){
					//Actualizacion usuario
					user.id = id;
					UserFactory.update(user);
					ToastService.show("Usuario Actualizado!");
				}else{
					//Crear usuario
					UserFactory.add(user);
					ToastService.show("Usuario creado!");
				}
				$scope.users = UserFactory.find();
				$scope.user = {};
			}
			$scope.remove = function(id){
				//Borrar usuario
				UserFactory.remove(id);
				ToastService.show("Usuario Borrado!");
				$scope.users = UserFactory.find();
			}
		}
	}
})();