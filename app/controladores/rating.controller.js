(function(){
	app.controller('RatingController', controller);
	function controller($scope, SecurityService, QualificationFactory, ComicFactory, SessionService){
		var id = $scope.$parent.comic.id;
		if(SecurityService.isUser()){
			
			var rating = QualificationFactory.find(function(q){
				return (q.user == SessionService.get('user').id)&&(q.comic==id);
			});
			if(rating)
				$scope.rating = rating.stars;			

			
			$scope.rate = function(){
				var previousRatingByUser = QualificationFactory.find(function(q){
					return (q.user == SessionService.get('user').id)&&(q.comic==id);
				});
				if(previousRatingByUser){
					
					previousRatingByUser.stars = $scope.rating;
					QualificationFactory.update(previousRatingByUser);
				}else{
					
					QualificationFactory.add({
						comic : id,
						user : SessionService.get('user').id,
						stars : $scope.rating
					});
				}
				$scope.$parent.$parent.$parent.justRated++;
			};
		}

		
		$scope.calculateRating = function(){		
			var quantity = 0; 
			var total = 0;

			QualificationFactory.find().forEach(function(q){
				if(q.comic==id){
					quantity++;
					total += q.stars;
				}
			});

			$scope.communityRating = Math.ceil(total/quantity);

			
			if($scope.$parent.comic.rating != $scope.communityRating){
				$scope.$parent.comic.rating = $scope.communityRating;
				ComicFactory.update($scope.$parent.comic);
			}

			$scope.stars = [
				{ name : "1 Estrella", value : 1 },
				{ name : "2 Estrella", value : 2 },
				{ name : "3 Estrella", value : 3 },
				{ name : "4 Estrella", value : 4 },
				{ name : "5 Estrella", value : 5 },
			];
		};

		
		$scope.calculateRating();
	}
})();