(function(){
	app.directive('breadcrumb', directive);
	function directive(){
		return {
			restrict : 'E',
			templateUrl : 'app/paginas/componentes/breadcrumb.html',
			link : function(scope, element, attrs){
				scope.links = JSON.parse(attrs.ngLinks);
			}
		}
	}
})();