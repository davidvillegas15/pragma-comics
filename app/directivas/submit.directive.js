(function(){
	app.directive('submit', directive);
	function directive(){
		return {
			restrict : 'E',
			templateUrl : 'app/paginas/componentes/submit.html',
			link : function(scope, element, attrs){
				scope.content=attrs.content;
			}
		}
	}
})();