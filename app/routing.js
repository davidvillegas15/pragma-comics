(function(){

	
	app.config(function($locationProvider){
		$locationProvider.hashPrefix("!");
	});

	//Rutas
	app.config(function($routeProvider){
		$routeProvider
		.when('/',{
			controller  : 'MainController',
			templateUrl : 'app/paginas/main.html'
		})
		.when('/sign-up',{
			controller  : 'SignUpController',
			templateUrl : 'app/paginas/sign-up.html'
		})
		.when('/administration',{
			templateUrl : 'app/paginas/administration.html'
		})
		.when('/comic/:id',{
			controller : 'ComicController',
			templateUrl : 'app/paginas/comic.html'
		})
		.when('/comics', {
			controller : 'ComicsController',
			templateUrl : 'app/paginas/comics.html'
		})
		.when('/my-account', {
			controller : 'MyAccountController',
			templateUrl : 'app/paginas/my-account.html'
		})
		.when('/comics/genre/:id', {
			controller : 'ComicsController',
			templateUrl : 'app/paginas/comics.html'
		})
		.when('/404',{
			templateUrl : 'app/paginas/404.html'
		})
		.otherwise({
			redirectTo : '/404'
		});
	});
})();