(function(){
	app.service('SignInValidator', validator);
	function validator(UserFactory){
		this.runValidation = function(scope){
			var messages = [];
			//Verificacion de usuario o email existen
			var userFound = UserFactory.find(function(user){
				//Encontrar usuario y nombre
				return (user.username==scope.user.username)&&(user.password==scope.user.password);
			});		
			if(!userFound)
				messages.push("Usuario o Clave equivocados");
			
			var isValid = userFound;
			if(isValid)
				messages.push("BIENVENIDO "+userFound.username+"!");
			return {
				isValid : isValid,
				messages : messages
			}
		}
	}
})();