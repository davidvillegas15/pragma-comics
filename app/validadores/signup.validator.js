(function(){
	app.service('SignUpValidator', validator);
	function validator(UserFactory){
		this.runValidation = function(scope){
			var messages = [];
			//Verificar si existe el email
			var alreadyExists = UserFactory.find(function(user){
				//Encontrar por email o usuario
				return (user.username==scope.user.username)||(user.email==scope.user.email);
			});
			if(alreadyExists)
				messages.push("Email ya existe.");
			//Verificacion de password
			var equalPasswords = (scope.user.password==scope.validation.password);
			if(!equalPasswords)
				messages.push("Claves no son iguales.");
			//Agregar Terminos de uso
			var agreement = scope.validation.agreement;
			if(!agreement)
				messages.push("Acepta los terminos de uso.");
		
			var isValid = !alreadyExists && equalPasswords && agreement;
			if(isValid)
				messages.push("Registro Exitoso!");
			return {
				isValid : isValid,
				messages : messages
			}
		}
	}
})();